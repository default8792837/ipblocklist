#!/usr/local/bin/bash

current_date=$(date +"%Y-%m-%d")

cd ${REPO_PATH}
git pull

python3 ipblocklist_gen.py '/var/log/suricata/eve.json.0' ipblocklist.txt

git add ipblocklist.txt
git commit -m "Automatic Blocklist update for $current_date"
git push https://${GIT_USERNAME}:${GIT_PASSWORD}@gitlab.com/default8792837/ipblocklist.git main
git fetch --all
