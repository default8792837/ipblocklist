import sys
import json

def parse_suricata_log(log_file):
    ip_set = set()
    with open(log_file, 'r') as file:
        for line in file:
            try:
                event = json.loads(line)
                if event.get('event_type') == 'alert':
                    src_ip = event.get('src_ip')
                    if src_ip:
                        ip_set.add(src_ip)
            except json.JSONDecodeError:
                continue
    return ip_set

def write_ip_blocklist(ip_set, output_file):
    with open(output_file, 'r') as file:
        existing_ips = set(line.strip() for line in file)

    with open(output_file, 'a') as file:
        for ip in ip_set:
            if ip not in existing_ips:
                file.write(ip + '\n')

def main():
    if len(sys.argv) != 3:
        print("Usage: python script.py <input_log_file> <output_file>")
        sys.exit(1)

    input_log_file = sys.argv[1]
    output_file = sys.argv[2]

    ip_set = parse_suricata_log(input_log_file)
    write_ip_blocklist(ip_set, output_file)

if __name__ == "__main__":
    main()
