# ipblocklist

## Motivation

I have been using Suricata on the WAN side of my OPNSense box for a while now

Unfortunately, IPS mode doesn't want to work with my setup (VM + PPPoE interface), so I created a Firewall Alias, a Floating rule (deny in) and manually added IPs to the alias, to block them

This is too much manual work and doesn't scale thus, I needed a way to dynamically block IP's based on the alerts from the IDS

## Implementation

1. Created a base blocklist.txt from my current alias
2. `ipblocklist_gen.py` parses the logfile and appends the IPs to the blocklist (alerts only)
3. `ipblocklist_cron.sh` runs the python script and handles git operations
4. The cronjob is configured via the built in OPNSense config daemon. Specifically:
- Created a new action `/usr/local/opnsense/service/conf/actions.d/actions_custom.conf`
```
[update]
command:/path/to/the/repo/ipblocklist_cron.sh
parameters:
type:script
message:update custom blocklist
description: Update custom blocklist
```
- Pass the git creds as environmental variables in `/usr/local/opnsense/service/conf/configd.conf.d/git.conf`
- Add the cronjob from the UI, selecting the action created above
5. Create an alias with type URL Table (IPs) and in the content add the raw upstream ipblocklist.txt url
6. Create a floating firewall rule that blocks all incoming traffic from the alias' IPs
